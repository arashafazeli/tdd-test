
from src.day_01 import get_data, increased ,sliding_windows, submarine_calculator, get_data_submarine, aim_submarine_calculator

################### day1 #######################

'''Advent of code day1 part1'''

def test_get_data():
    assert get_data("depth.txt") != 0

def test_increased():
    assert increased("depth.txt") >= 0

''' Advent of code day1 part2'''

def test_sliding_windows():
    assert sliding_windows("depth.txt") >= 0

################### day2 #######################

'''Advent of code day2 part1'''

def test_get_data_submarine():
    assert str(get_data_submarine("commands.txt")) != 0
    
def test_submarine_calculator():
    assert submarine_calculator("commands.txt") >= 0

'''Advent of code day2 part2'''

def test_aim_submarine_calculator():
    assert aim_submarine_calculator("commands.txt") >= 0
