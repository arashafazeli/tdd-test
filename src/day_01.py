
################### day1 ########################

def get_data(filename):
    data = open(filename, 'r', encoding='utf-8').read().splitlines()
    depths = [(x) for x in data]
    return depths


def increased(filename):
    depths = get_data(filename)
    increases = sum(x < y for x, y in zip(depths, depths[1:]))
    return increases

# puzzle answer was 1791.

def sliding_windows(filename):
    depths = get_data(filename)
    increases = sum(x < y for x, y in zip(depths, depths[3:]))
    return increases

# puzzle answer was 1822.

##################### day2 #######################

def get_data_submarine(filename):
    data = open(filename, 'r', encoding='utf-8').read().splitlines()
    data = [x.split() for x in data]
    commands = [(x[0], int(x[1])) for x in data]
    return commands

def submarine_calculator(filename):
    commands = get_data_submarine(filename)
    h = 0
    d = 0
    for cmd, val in commands:
        if cmd == 'forward':
            h += val
        elif cmd == 'down':
            d += val
        elif cmd == 'up':
            d -= val
        else:
            raise ValueError('Invalid command')
    return(h * d)

 # puzzle answer was 1813801.  

def aim_submarine_calculator(filename):
    commands = get_data_submarine(filename)
    aim, h, d = 0, 0, 0

    for cmd, val in commands:
        if cmd == 'forward':
            h += val
            d += val * aim
        elif cmd == 'down':
            aim += val
        elif cmd == 'up':
            aim -= val
        else:
            raise ValueError('Invalid command')

    return(h * d)

## puzzle answer was  1960569556